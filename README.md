关于 Yii2 elasticsearch log
==================


本项目是Yii2日志扩展 将日志写入到elasticsearch中


## 安装

```php
    composer require qiumoon/eslog
```

## 使用方法

> Yii2配置文件中添加
```php
    'components' => [
        //修改Log类
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [       
                [
                    'class'=>'qiumoon\EsTarget',
                    'levels'=>['error','warning','info']    ,
                    'logVars' => [
                        '_GET',
                        '_POST',
                        '_FILES',                        
                    ]         
                ]                
            ],
        ],
        //加入elasticsearch
        'elasticsearch' => [
            'class' => 'yii\elasticsearch\Connection',
            'nodes' => [
                ['http_address' => '172.20.0.1:9200'],
                // configure more hosts if you have a cluster
            ],
            'autodetectCluster'=>false
        ],
    ]
```