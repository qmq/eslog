<?php
namespace qiumoon\model;
use yii\elasticsearch\ActiveRecord;
class Log extends ActiveRecord {
    /** 索引名称 */
    public static function index(){        
        return \Yii::$app->id;
    }
    
    /** 类型 */
    public static function type(){
        return "log";
    }

    public function attributes(){
        return ['msg'];
    }
    
    public function afterValidate(){
        parent::afterValidate();
        $this->primaryKey = microtime();        
    }
}